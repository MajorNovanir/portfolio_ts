module.exports = {
  presets: [
    ["next/babel", { "preset-react": { runtime: "automatic", importSource: "react" } }],
    "@babel/preset-typescript",
  ],
  plugins: [
    ["styled-components", { ssr: true }],
  ],
};
